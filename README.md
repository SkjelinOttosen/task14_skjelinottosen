A simple customer API dockerized.
To correctly use the Customer API, you'll need to configure a docker-compose.yml file
that creates a bridged network between the two images (so they can communicate with each other).

Main API Endpoint: localhost:5150/api/customers
Endpoints for `customer` (localhost:5150/api/customers):
    - GET: Retrieve all customers
    - GET:{id} Retrieve customer by id
    - POST: Add a new customer
    - PUT: Update a customer
    - Delete: Delete a customer

Docker images
API: https://hub.docker.com/r/skjelinottosen/task14
Database: https://hub.docker.com/r/skjelinottosen/task14_db