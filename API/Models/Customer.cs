﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Task14_SkjelinOttosen.Models
{
    public class Customer
    {
        // Primary key
        [Key]
        public Guid CustomerId { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [MaxLength(95)]
        public string Address { get; set; }
        [MaxLength(15)]
        public string PhoneNumber { get; set; }
        [MaxLength(64)]
        public string Email { get; set; }
    }
}
