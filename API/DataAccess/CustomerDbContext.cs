﻿using Microsoft.EntityFrameworkCore;
using Task14_SkjelinOttosen.Models;

namespace Task14_SkjelinOttosen.DataAccess
{
    public class CustomerDbContext : DbContext
    { 
        public DbSet<Customer> Customers { get; set; }

        public CustomerDbContext(DbContextOptions<CustomerDbContext> options) : base(options)
        {
            // Creates a new database if it does not already exist
            if (!Database.CanConnect())
            {
                Database.EnsureCreated();
            }     
        }
    }
}
